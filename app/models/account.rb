class Account < ActiveRecord::Base

  after_initialize :init_balance


  def deposit_amount(amount)
    if amount > 0
      self.balance += amount
    else
      false
    end
  end

  def withdraw_amount(amount)
    if amount > 0
      if amount < self.balance
        self.balance -= amount
      else
        false
      end
    else
      false
    end
  end

  def interest_charge(amount)
    if amount > 200
      self.balance = amount + 10
    elsif amount < 5
      self.balance = 0
    elsif amount < 25
      self.balance = amount - 5
    else
      self.balance = amount
    end
  end

  private

  def init_balance
    self.balance ||= 0
  end


end
