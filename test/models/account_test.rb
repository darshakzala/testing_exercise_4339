require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account = Account.new
  end

  test "An account with no deposits should return 0 for its amount" do
    assert_equal 0,@account.balance
  end

  test "An account with one deposit should have a balance equal to that deposit" do
    @account.deposit_amount(10)
    assert_equal 10, @account.balance
  end

  test "Withdrawing from the account should alter the balance" do
    @account.deposit_amount(100)
    assert @account.withdraw_amount(10)
    assert_equal 90, @account.balance
  end

  test "A user cannot withdraw more than what is in the account" do
    @account.deposit_amount(10)
    assert  !@account.withdraw_amount(100), "Withdrawal incorrectly allowed"
    assert_equal 10, @account.balance
  end

  test "A Deposit cannot be negative" do
    @account.deposit_amount(-5)
    assert !@account.deposit_amount(-5), "Deposit is a negative number"
    assert_equal 0, @account.balance
  end

  test "A Withdrawal cannot be negative" do
    @account.withdraw_amount(-5)
    assert !@account.withdraw_amount(-5), "Withdrawal is a negative number"
    assert_equal 0, @account.balance
  end

  test "Balance > $200.00 will have interest of $10.00 added" do
    @account.deposit_amount(10)
    assert @account.interest_charge(205), "Interest of $10.00 is added"
    assert_equal 215, @account.balance
  end

  test "Balance < $25.00 will have $5.00 charged" do
    @account.withdraw_amount(5)
    assert @account.interest_charge(24), "Charge of $5.00 is subtracted"
    assert_equal 19, @account.balance
  end

  test "Balance < $5.00 will reduce account to $0.00" do
    assert @account.interest_charge(4), "Balance < $5.00 will make balance $0.00"
    assert_equal 0, @account.balance
  end
end
